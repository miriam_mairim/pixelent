﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPositions : MonoBehaviour
{
	public List<List<Vector3>> Teleports = new List<List<Vector3>>();

	void Awake () {

		foreach (Transform teleport in gameObject.transform)
		{
			var teleportList = new List<Vector3>();
			foreach (Transform child in teleport)
			{
				teleportList.Add(child.position);
			}
			Teleports.Add(teleportList);
		}
	}
	
}
