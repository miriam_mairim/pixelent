﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour
{
	[SerializeField] private GameObject _mCharacter;
	[SerializeField] private ObstaclePositions _mObstacles;
	[SerializeField] private PipePositions _mPipes;
	[SerializeField] private TeleportPositions _mTeleports;
	[SerializeField] private GameObject _mGameOverPanel;
	[SerializeField] private Button _mRestartButton;
	[SerializeField] private Button _mBackButton;

	[SerializeField] private Text _mUpCount;
	[SerializeField] private Text _mDownCount;
	[SerializeField] private Text _mRightCount;
	[SerializeField] private Text _mLeftCount;


	public Vector3 Direction = Vector3.zero;
	public bool IsTargetReached;
	public bool IsInThePipe;
	public bool GameOver;

	//arrow inventory
	[SerializeField] public int UpCount = 5;
	[SerializeField] public int DownCount = 5;
	[SerializeField] public int RightCount = 5;
	[SerializeField] public int LeftCount = 5;

	private List<Vector3> mCurrentPipe;
	private int mCurrentPipeIndex;

	private const float MinX = -8f;
	private const float MaxX = 8f;
	private const float MinY = -10f;
	private const float MaxY = 16f;

	void Start()
	{
		InvokeRepeating("ChangePosition", 1, 0.5f);

		_mUpCount.text = UpCount.ToString();
		_mDownCount.text = DownCount.ToString();
		_mRightCount.text = RightCount.ToString();
		_mLeftCount.text = LeftCount.ToString();

		if (UpCount == 0)
		{
			_mUpCount.color = Color.red;
		}
		if (DownCount == 0)
		{
			_mDownCount.color = Color.red;
		}
		if (RightCount == 0)
		{
			_mRightCount.color = Color.red;
		}
		if (LeftCount == 0)
		{
			_mLeftCount.color = Color.red;
		}

		_mRestartButton.onClick.AddListener(Restart);
		_mBackButton.onClick.AddListener(Back);
	}


	void Update()
	{
		if(GameOver) return;
		GameOver = IsGameOver();

		foreach (var teleportPosition in _mTeleports.Teleports)
		{
			if (teleportPosition[0] == transform.position && !IsInThePipe)
			{
				transform.position = teleportPosition[1];
			}
		}

		foreach (var pipePosition in _mPipes.Pipes)
		{
			if (pipePosition[0] == transform.position && !IsInThePipe)
			{
				IsInThePipe = true;
				mCurrentPipe = pipePosition;
				mCurrentPipeIndex = 0;
			}
		}

		if (IsInThePipe)
		{
			if (mCurrentPipe[mCurrentPipeIndex] != transform.position)
			{
				return;
			}

			if (mCurrentPipeIndex == mCurrentPipe.Count - 1)
			{
				mCurrentPipe = null;
				IsInThePipe = false;
				return;
			}

			if (transform.position.x < mCurrentPipe[mCurrentPipeIndex+1].x)
			{
				Direction = Vector3.right;
			}
			else if (transform.position.x > mCurrentPipe[mCurrentPipeIndex+1].x)
			{
				Direction = Vector3.left;
			}
			else if (transform.position.y > mCurrentPipe[mCurrentPipeIndex+1].y)
			{
				Direction = Vector3.down;
			}
			else if (transform.position.y < mCurrentPipe[mCurrentPipeIndex+1].y)
			{
				Direction = Vector3.up;
			}

			//flip the animation to the right and left
			transform.GetChild(0).localScale = Direction == Vector3.left ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);

			mCurrentPipeIndex++;
		}

		if (IsInThePipe) return;

		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			SpendArrow(Vector3.up);
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			SpendArrow(Vector3.right);
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			SpendArrow(Vector3.down);
		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			SpendArrow(Vector3.left);
		}
	}

	private void ChangePosition()
	{
		if (IsTargetReached) return;

		//prevent moving if there is an obstacle
		if (_mObstacles.OccupiedPositions.Contains(transform.position + Direction * 2))
		{
			transform.GetChild(0).GetComponent<Animator>().enabled = false;
			return;
		}

		//prevent entering the pipe from other directions
		if (!IsInThePipe)
		{
			foreach (var pipe in _mPipes.Pipes)
			{
				for (var pos = 1; pos < pipe.Count; pos++)
				{
					if (pipe[pos] == transform.position + Direction * 2)
					{
						transform.GetChild(0).GetComponent<Animator>().enabled = false;
						return;
					}
				}
			}
		}

		transform.position += Direction * 2;


		transform.GetChild(0).GetComponent<Animator>().enabled = !(transform.position.x < MinX || transform.position.x > MaxX ||
		                                                          transform.position.y < MinY || transform.position.y > MaxY);

		var newX = Mathf.Clamp(transform.position.x, MinX, MaxX);
		var newY = Mathf.Clamp(transform.position.y, MinY, MaxY);
		var newPos = new Vector3(newX, newY);
		transform.position = newPos;

	}

	public void SpendArrow(Vector3 direction)
	{
		//do not spend if you are already going that direction
		if(Direction == direction) return;

		if (direction == Vector3.up)
		{
			if (UpCount == 0)
			{
				return;
			}
			UpCount--;
			if (UpCount == 0)
			{
				_mUpCount.color = Color.red;
			}
			_mUpCount.text = UpCount.ToString();
		}
		else if (direction == Vector3.down)
		{
			if (DownCount == 0)
			{
				return;
			}
			DownCount--;
			if (DownCount == 0)
			{
				_mDownCount.color = Color.red;
			}
			_mDownCount.text = DownCount.ToString();
		}
		else if (direction == Vector3.right)
		{
			if (RightCount == 0)
			{
				return;
			}
			RightCount--;
			if (RightCount == 0)
			{
				_mRightCount.color = Color.red;
			}
			_mRightCount.text = RightCount.ToString();
		}
		else if (direction == Vector3.left)
		{
			if (LeftCount == 0)
			{
				return;
			}
			LeftCount--;
			if (LeftCount == 0)
			{
				_mLeftCount.color = Color.red;
			}
			_mLeftCount.text = LeftCount.ToString();
		}

		Direction = direction;

		FlipAnimationRotation();
	}

	private void FlipAnimationRotation()
	{
		//flip the animation to the right and left
		transform.GetChild(0).localScale = Direction == Vector3.left ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
	}

	private bool IsGameOver()
	{
		if (!IsTargetReached && !IsInThePipe)
		{
			if (Direction == Vector3.right)
			{
				//TODO check if any ladders on the way to lead you to up
				//or just fake it *shush*
				if (UpCount == 0)
				{
					if (SceneManager.GetActiveScene().name != "HardScene")
					{
						if (transform.position.y < MaxY)
						{
							ShowGameOver();
							return true;
						}
					}
					else
					{
						if (transform.position.y < 2)
						{
							ShowGameOver();
							return true;
						}
					}
				}
			}
			else if (Direction == Vector3.up)
			{
				if (RightCount == 0)
				{
					if (transform.position.x < MaxX)
					{
						ShowGameOver();
						return true;
					}
				}
			}
			else if (Direction == Vector3.left)
			{
				if (RightCount == 0)
				{
					ShowGameOver();
					return true;
				}
			}
			else if (Direction == Vector3.down)
			{
				if (SceneManager.GetActiveScene().name != "MediumScene")
				{
					if (UpCount == 0 || (RightCount == 0 && transform.position.x < MaxX))
					{
						ShowGameOver();
						return true;
					}
				}
			}
		}

		return false;
	}

	private void ShowGameOver()
	{
		var graphicsComponents = _mGameOverPanel.GetComponentsInChildren<Graphic>();
		_mGameOverPanel.SetActive(true);

		foreach (var component in graphicsComponents)
		{
			GraphicExtensions.FadeIn(component, 0.5f);
		}

		GraphicExtensions.FadeIn(_mGameOverPanel.GetComponent<Graphic>(), 0.5f);
	}

	private void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	private void Back()
	{
		SceneManager.LoadScene("MainMenuScene");
	}
}
