﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalChecker : MonoBehaviour {

	[SerializeField] private GameObject _mGoal;
	[SerializeField] private CharacterMovement _mCharacterMovement;
	[SerializeField] private GameObject _mSuccessPanel;


	[SerializeField] private AudioSource _mSuccessSound;

	void Update()
	{
		if (!_mCharacterMovement.IsTargetReached && _mGoal.transform.position == transform.position)
		{
			Debug.Log("WIN");
			_mCharacterMovement.Direction = Vector3.zero;
			_mCharacterMovement.IsTargetReached = true;

			_mSuccessSound.Play();

			var graphicsComponents = _mSuccessPanel.GetComponentsInChildren<Graphic>();
			_mSuccessPanel.SetActive(true);

			foreach (var component in graphicsComponents)
			{
				GraphicExtensions.FadeIn(component, 0.5f);
			}

			GraphicExtensions.FadeIn(_mSuccessPanel.GetComponent<Graphic>(), 0.5f);
		}
	}
}

public static class GraphicExtensions
{
	public static void FadeIn(this Graphic g, float time = 2f)
	{
		g.GetComponent<CanvasRenderer>().SetAlpha(0f);
		g.CrossFadeAlpha(1f, time, false);
	}
	public static void FadeOut(this Graphic g)
	{
		g.GetComponent<CanvasRenderer>().SetAlpha(1f);
		g.CrossFadeAlpha(0f, .3f, false);
	}

}