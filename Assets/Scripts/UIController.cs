﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	[SerializeField] private Button _mButtonUp;
	[SerializeField] private Button _mButtonRight;
	[SerializeField] private Button _mButtonDown;
	[SerializeField] private Button _mButtonLeft;

	[SerializeField] private Button _mStartLevel;
	[SerializeField] private GameObject _mStartPanel;
	[SerializeField] private Button _mNextLevel;

	[SerializeField] private AudioSource _mClickSound;

	[SerializeField] private CharacterMovement _mCharacter;

	void Awake()
	{
		_mButtonUp.onClick.AddListener(delegate { ChangeDirection(Vector3.up); });
		_mButtonRight.onClick.AddListener(delegate { ChangeDirection(Vector3.right); });
		_mButtonDown.onClick.AddListener(delegate { ChangeDirection(Vector3.down); });
		_mButtonLeft.onClick.AddListener(delegate { ChangeDirection(Vector3.left); });

		_mNextLevel.onClick.AddListener(LoadNextScene);
		_mStartLevel.onClick.AddListener(StartLevel);
	}

	private void ChangeDirection(Vector3 direction)
	{
		if(_mCharacter.IsInThePipe) return;

		_mClickSound.Play();
		_mCharacter.SpendArrow(direction);
	}

	private void LoadNextScene()
	{
		var nextIndex = SceneManager.GetActiveScene().buildIndex + 1;
		nextIndex = Mathf.Clamp(nextIndex, 0, SceneManager.sceneCount - 1);
		SceneManager.LoadScene(nextIndex);
	}

	private void StartLevel()
	{
		_mCharacter.Direction = Vector3.right;

		var graphicsComponents = _mStartPanel.GetComponentsInChildren<Graphic>();
		_mStartPanel.SetActive(false);

		foreach (var component in graphicsComponents)
		{
			GraphicExtensions.FadeOut(component);
		}

		GraphicExtensions.FadeOut(_mStartPanel.GetComponent<Graphic>());
	}
}
