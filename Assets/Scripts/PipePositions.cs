﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipePositions : MonoBehaviour
{
	public List<List<Vector3>> Pipes = new List<List<Vector3>>();

	void Awake () {

		foreach (Transform pipe in gameObject.transform)
		{
			var pipeList = new List<Vector3>();
			foreach (Transform child in pipe)
			{
				pipeList.Add(child.position);
			}
			Pipes.Add(pipeList);
		}
	}
	
}
