﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

	[SerializeField] private Button _mEasyButton;
	[SerializeField] private Button _mMediumButton;
	[SerializeField] private Button _mHardButton;

	[SerializeField] private AudioSource _mClickSound;

	void Awake()
	{
		_mEasyButton.onClick.AddListener(GoToEasyLevel);
		_mMediumButton.onClick.AddListener(GoToMediumLevel);
		_mHardButton.onClick.AddListener(GoToHardLevel);
	}

	private void GoToEasyLevel()
	{
		_mClickSound.Play();

		Invoke("LoadEasy", 0.6f);
	}

	private void GoToMediumLevel()
	{
		_mClickSound.Play();

		Invoke("LoadMedium", 0.6f);
	}

	private void GoToHardLevel()
	{
		_mClickSound.Play();

		Invoke("LoadHard", 0.6f);
	}

	private void LoadEasy()
	{
		SceneManager.LoadScene("EasyScene");
	}

	private void LoadMedium()
	{
		SceneManager.LoadScene("MediumScene");
	}

	private void LoadHard()
	{
		SceneManager.LoadScene("HardScene");
	}
}
