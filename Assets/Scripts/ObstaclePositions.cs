﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePositions : MonoBehaviour
{
	public List<Vector3> OccupiedPositions;

	void Awake () {

		foreach (Transform child in gameObject.transform)
		{
			OccupiedPositions.Add(child.position);
		}
	}
	
}
